﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using WebParseApp.Models;
using WebParser;
using static WebParser.Load.DocumentNode;

namespace WebParseApp.Controllers
{
    public class HomeController : Controller
    {
        public IActionResult Index()
        {    
            return View();
        }

        [HttpPost]
        public IActionResult Index(string url, string tag)
        {
            var web = new ParseSite();
            var t = web.Load(url);
            ViewBag.Web = t.Document.GetTagList(tag);
            return View();
        }

        public IActionResult About()
        {
            ViewData["Message"] = "Your application description page.";

            var web = new ParseSite();
            var t = web.Load("http://slusar.su");
            var tagList = t.Document.GetTagList("");
            //var z = y.Where(x => x.Attributes.Any(o => o.Name == "class" )).Select(c => c.Content).ToArray();           
            //var Content = t.Document.GetContentBySelector("entry-date");
            //var result = tagList.Where(x => x.Attributes.Any(o => o.Value.Contains("entry-date")))
            //    .Select(c => c.Content)
            //    .ToArray();
            var result = t.Document.GetContentsByTagSelector("time", "entry-date");
            ViewBag.Any = result;
            return View();
        }

        public IActionResult Contact()
        {
            ViewData["Message"] = "Your contact page.";

            ViewBag.Id = Guid.NewGuid();
            ViewBag.AccountId = Guid.NewGuid();
            MD5 md5Hash = MD5.Create();
            var source = "TTTotal";
            ViewBag.Hash = GetMd5Hash(md5Hash, source);

            return View();
        }

        static string GetMd5Hash(MD5 md5Hash, string input)
        {

            // Convert the input string to a byte array and compute the hash.
            byte[] data = md5Hash.ComputeHash(Encoding.UTF8.GetBytes(input));

            // Create a new Stringbuilder to collect the bytes
            // and create a string.
            StringBuilder sBuilder = new StringBuilder();

            // Loop through each byte of the hashed data 
            // and format each one as a hexadecimal string.
            for (int i = 0; i < data.Length; i++)
            {
                sBuilder.Append(data[i].ToString());
            }

            // Return the hexadecimal string.
            return sBuilder.ToString();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
