﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Text.RegularExpressions;

namespace WebParser
{
    public class Load
    {
        static string Url { get; set; }
        static WebClient Client { get; set; }
        public string HtmlText { get; set; }
        public List<string> HtmlList { get; set; }
        public DocumentNode Document { get; set; }

        public Load(string url)
        {
            Url = url;
            HtmlText = DocumentText();
            HtmlList = DocumentList();
            Document = new DocumentNode(HtmlText);
        }

        public class DocumentNode
        {
            public Dictionary<string, Tag> Tags { get; set; }
            public string Html { get; set; }
            public List<string> TagsList { get; set; }

            public DocumentNode(string HtmlText)
            {
                Html = HtmlText;
                TagsList = GetTagsList(Html);
            }

            public class Tag
            {
                public string Content { get; set; }
                public List<string> Class { get; set; }
                public string Id
                {
                    get => Attributes.Where(x => x.Key == "id").Select(x => x.Value).FirstOrDefault();
                    set
                    {
                        if (Attributes.ContainsKey("id"))
                            Attributes["id"] = value;
                        else
                            Attributes.Add("id", value);
                    }
                }
                public string Name { get; set; }
                public AttributesDictionary Attributes { get; set; }
                public List<Tag> Child { get; set; }
            }

            public class TagList
            {
                public string Content { get; set; }
                public string Name { get; set; }
                public List<Attribute> Attributes { get; set; }
            }

            public class Attribute
            {
                public string Name { get; set; }
                public List<string> Value { get; set; }
            }  

            public List<string> GetTagsList(string text)
            {
                var tagsList = new List<string>();
                string pattern = @"<.*>.*</.*>";
                var matches = Regex.Matches(text, pattern);
                for (int i = 0; i < matches.Count; i++)
                {
                    tagsList.Add(matches[i].ToString());
                }
                return tagsList;
            }

            public IEnumerable<TagList> GetTagList(string tagName)
            {
                var tagList = new List<TagList>();
                var regString = $"<{tagName}.*?>(.*?)</{tagName}.*>";
                if (string.IsNullOrEmpty(tagName))
                {
                    regString = $"<.*?>(.*?)</.*>";
                }
                var rx = new Regex(regString);
                var matches = rx.Matches(Html);

                if (matches.Count > 0)
                {
                    var count = 0;
                    foreach (var item in matches)
                    {
                        var match = matches[count];
                        var groupCollection = match.Groups;
                        var group = groupCollection[0].ToString().Split(">").ToArray();

                        var tag = new TagList
                        {
                            Name = tagName,
                            Content = groupCollection[1].ToString(),
                            Attributes = GetAttributes(group[0])
                    };
                        tagList.Add(tag);
                        count++;
                    }
                }
                return tagList;
            }

            public List<Attribute> GetAttributes(string line)
            {
                var attributes = new List<Attribute>();
                var rx = new Regex(@"\w*\s*=\s*"".*?""");
                var matches = rx.Matches(line);
                
                if (matches.Count > 0)
                {
                    var count = 0;
                    foreach (var item in matches)
                    {
                        var itemList = item.ToString().Split("=");
                        var atrValues = new List<string>();
                        var name = itemList[0].Replace(@"""", "");
                        var value = itemList[1].Replace(@"""", "").Replace(@"\", "");
                        if (name == "class")
                        {
                            var classList = value.Split(" ");
                            foreach (var classItem in classList)
                            {
                                atrValues.Add(classItem);
                            }
                        }
                        else
                        {
                           atrValues.Add(value);
                        }
                        
                        var atr = new Attribute
                        {
                            Name = name,
                            Value = atrValues,
                        };
                        attributes.Add(atr);
                    }
                }
                return attributes;
            }


            public IEnumerable<string> GetContentsByTagSelector(string tagName, string selector)
            {
                var tagList = GetTagList(tagName);
                var result = tagList.Where(x => x.Attributes.Any(o => o.Value.Contains(selector)))
                .Select(c => c.Content)
                .ToArray();
                return result;
            }

            public IEnumerable<string> GetContentBySelector(string selector)
            {
                var tagList = GetTagList("");
                var result = tagList.Where(x => x.Attributes.Any(o => o.Value.Contains(selector)))
                .Select(c => c.Content)
                .ToArray();
                return result;
            }

        }

        protected string DocumentText()
        {
            var htmlCode = string.Empty;

            using (HttpClient client = new HttpClient())
            {
                using (HttpResponseMessage response = client.GetAsync(Url).Result)
                {
                    using (HttpContent content = response.Content)
                    {
                        return htmlCode = content.ReadAsStringAsync().Result;
                    }
                }
            }
        }

        protected List<string> DocumentList()
        {
            var lines = new string[0] { };

            if (string.IsNullOrEmpty(HtmlText))
            {
                lines = HtmlText.Split(new[] { Environment.NewLine }, StringSplitOptions.None);
                return lines.ToList();
            }
            return lines.ToList();
        }

    }
}
