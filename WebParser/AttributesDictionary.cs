﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WebParser
{
    public class AttributesDictionary : Dictionary<string, string>
    {
        public new void Add(string key, string value)
        {
            base.Add(key.ToLower(), value);
        }
    }
}
